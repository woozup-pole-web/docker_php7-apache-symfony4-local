#!/bin/bash
awk 'END{print $1}' /etc/hosts > /root/ip
ip=`cat /root/ip`
rm /root/ip
sed -i "s/DOCKER_REPLACE_IP/IP.1 = $ip/" /root/openssl.cnf
openssl genrsa -des3 -passout pass:Z9mSrAfeD -out /root/certs/server.pass.key 2048
openssl rsa -passin pass:Z9mSrAfeD -in /root/certs/server.pass.key -out /root/certs/server.key
rm /root/certs/server.pass.key
openssl req -new -key /root/certs/server.key -out /root/certs/server.csr -config /root/openssl.cnf\
       	-subj "/C=FR/ST=Bas-Rhin/L=Strasbourg/O=PG1/CN=${DOMAIN_NAME}"
openssl x509 -req -days 365 -in /root/certs/server.csr -signkey /root/certs/server.key -out /root/certs/server.crt.pem -extensions v3_req -extfile /root/openssl.cnf
a2ensite default-ssl
service apache2 reload

service incron start

sed -i "s/myhostname = .*/myhostname = ${DOMAIN_NAME}/" /etc/postfix/main.cf
echo "${DOMAIN_NAME}" > /etc/mailname
service postfix reload
service postfix start

exec "$@"
